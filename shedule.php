<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: X-Requested-With');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

// get the HTTP method, path and body of the request
$method = $_SERVER['REQUEST_METHOD'];
$request = explode('/', trim($_SERVER['PATH_INFO'], '/'));
$input = json_decode(file_get_contents('php://input'), true);

// connect to the mysql database
$link = mysqli_connect('localhost', 'y80', '191980', 'y80_taxi');
mysqli_set_charset($link, 'utf8');

// retrieve the table and key from the path
$key = preg_replace('/[^a-z0-9_]+/i', '', array_shift($request));
$table = array_search($key, array('spr_fam' => 'Fam', 'spr_name' => 'Name', 'spr_otch' => 'Otch',
    'spr_gragdanstvo' => 'gragd', 'spr_pasp_kem_vydan' => 'kem_vydan', 'spr_oblast' => 'Oblast', 'spr_raion' => 'Raion',
    'spr_gorod' => 'Gorod', 'spr_nasel_punkt' => 'Nasel_Punkt', 'spr_ulici' => 'Ulici',
    'spr_org' => 'name_org', 'spr_mesto_raboty' => 'nazv_podrazdelenija', 'spr_profesija' => 'professija',
    'profvrednost_prilozh' => 'nomer', 'spr_prichina' => 'Prich'));
if (!$table) {
    $table = $key;
    $key = '';
}
//$key = array_shift($request);//+0;
$fval = array_shift($request);//+1;
$fval2 = array_shift($request);//+2;
$fval3 = array_shift($request);//+3;
$fval4 = array_shift($request);//+4;

// create SQL based on HTTP method
switch ($method) {
    case 'GET':
        $sql = "select * from `$table`" . ($key ? " WHERE LOWER($key) Like '%" . strtolower($fval) . "%'" . " LIMIT 10" : '');
        break;
    case 'PUT':
        $sql = "update `$table` set $set where id=$key";
        break;
    case 'POST':
        if ($table == "mvc") {
            if (isset($_POST["id"])) {//upd
                $sql = "update mvc set fio='". $_POST["fio"] ."', email='". $_POST["email"] ."', text='".
                    $_POST["texts"]. "', stat='". ($_POST["stat"]=='on'?1:0) .
                    "', adm='". ($_POST["adm"]=='on'?1:0) ."' where id=" . $_POST["id"];
            } else if (isset($_POST["del"])) {//del
                $sql = "delete from mvc where id=" . $_POST["del"];
            } else {
                if(isset($_POST)) {
                    $sql = "INSERT INTO mvc (fio, email, text) VALUES (" .
                        "'" . $_POST["fio"] . "','" . $_POST["email"] . "','" . $_POST["text"] . "');";
                }
            }
        }
        break;
    case 'DELETE':
        $sql = "delete `$table` where id=$key";
        break;
}

// excecute SQL statement
$result = mysqli_query($link, $sql);

// die if SQL statement failed
if (!$result) {
    http_response_code(404);
    die(mysqli_error());
}

// print results, insert id or affected row count
if ($method == 'GET') {
    if (mysqli_num_rows($result) > 1) echo '[';
    for ($i = 0; $i < mysqli_num_rows($result); $i++) {
        echo ($i > 0 ? ',' : '') . json_encode(mysqli_fetch_object($result));
    }
    if (mysqli_num_rows($result) > 1) echo ']';
} elseif ($method == 'POST') {
	$pos_ins = mysqli_insert_id($link);
	$kl = isset($kl_ins) ? $kl_ins: 0;
    if ($table != "mvc") {
        echo json_encode(array('aff' => mysqli_affected_rows($link), 'pos' => $pos_ins,
            'id' => mysqli_insert_id($link), 'kl' => $kl, 'sql' => $sql));
    }else{
        $redirect = isset($_SERVER['HTTP_REFERER'])? $_SERVER['HTTP_REFERER']:'redirect-form.html';
        header("Location: $redirect");
    }
} else {
    echo mysqli_affected_rows($link);
}

// close mysql connection
mysqli_close($link);
?>

