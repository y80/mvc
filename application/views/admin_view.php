<div class="container">
    <h2>Панель администрирования</h2>
    <table class="table table-striped table-sm"
           id="table"
           data-id-field="id"
           data-toggle="table"
           data-search="true"
           data-height="460"
           data-sort-name="name"
           data-locale="ru-RU"
           data-pagination="true"
           data-page-size="3"
           data-page-list="[3, 10, 25, 50, 100, 200, All]"
           data-single-select="true"
           data-click-to-select="true"
           data-url="http://uokcpp.ru/shedule.php/mvc"
           >
        <thead class="thead-dark">
        <tr>
            <th data-field="state" data-checkbox="true"></th>
            <th data-field="id" data-sortable="true">ID</th>
            <th data-field="fio" data-sortable="true">имя пользователя</th>
            <th data-field="email" data-sortable="true">е-mail</th>
            <th data-field="text">текст задачи</th>
            <th data-field="stat" data-sortable="true" data-formatter="statFormatter">статус</th>
        </tr>
        </thead>
    </table>

    <button type="button" class="btn btn-primary" onclick="edit_row()" data-toggle="modal" data-target="#myModal">
        Редактировать
    </button>
    <button type="button" class="btn btn-secondary" onclick="delete_row()" data-toggle="modal">
        Удалить
    </button>
</div>

<!-- The Modal -->
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="http://uokcpp.ru/shedule.php/mvc" method="post">
            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Редактировать задачу</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                    <input style="display: none" type="text" class="form-control" id="idr" name="id">
                    <div class="form-group">
                        <label for="fio">имя пользователя:</label>
                        <input type="text" class="form-control" id="fio" placeholder="имя пользователя" name="fio"
                               required readonly>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email"
                               required readonly>
                    </div>
                    <div class="form-group">
                        <label for="texts">текст задачи:</label>
                        <input type="text" class="form-control" id="texts" placeholder="текст задачи" name="texts" required>
                    </div>
                    <div class="form-group form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" id="stat" type="checkbox" name="stat"> Выполнено
                        </label>
                        
                        <label class="form-check-label" style="display: none">
                            <input class="form-check-input" id="adm" type="checkbox" name="adm"> change
                        </label>
                    </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
            </div>
        </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var id, row, selectionRow, changeText = false;

    var $adm = document.getElementById('texts'); 
    $adm.oninput = function() {
        if(!changeText) {
            var adm = document.querySelector('#adm');
            adm.checked = 1;
            changeText = true;
        }        
    };

    function delete_row() {        
        if(selectionRow) {
            $.post("http://uokcpp.ru/shedule.php/mvc", {del: selectionRow.id}).done(function (data) {
                document.location.reload(true);                
            });
        }else {
        }
    }

    function statFormatter(value, row) {
        var stat = value === '1' ? 'выполнено' : '';
        stat += row.adm === '1' ? ', <small>отредактировано администратором</small>' : '';
        return  stat;
    }    

    function refresh() {
        $table.bootstrapTable('refresh');
    }

    function edit_row(){
        changeText = false
        var adm = document.querySelector('#adm');
        adm.checked = +selectionRow.adm;
        var fio = document.querySelector('#fio');
        var email = document.querySelector('#email');
        var texts = document.querySelector('#texts');
        var stat = document.querySelector('#stat');
        var idr = document.querySelector('#idr');
        idr.value = selectionRow.id; 
        fio.value = selectionRow.fio; 
        email.value = selectionRow.email; 
        texts.value = selectionRow.text; 
        stat.checked = +selectionRow.stat; 
    }    

    var $table = $('#table')
    var $remove = $('#remove')
    var selections = []

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            selectionRow = row;
            return row.id
        })
    }

    $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table',
        function () {
            selections = getIdSelections()
        })
</script>
