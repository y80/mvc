<div class="container" style="height: 70vh">
    <table class="table table-striped"
           id="table"
           data-id-field="id"
           data-toggle="table"
           data-search="true"           
           data-sort-name="name"
           data-locale="ru-RU"
           data-pagination="true"
           data-page-size="3"
           data-page-list="[3, 10, 25, 50, 100, 200, All]"
           data-url="http://uokcpp.ru/shedule.php/mvc" >
        <thead class="thead-dark">
        <tr>
            <th data-field="fio" data-sortable="true">имя пользователя</th>
            <th data-field="email" data-sortable="true">е-mail</th>
            <th data-field="text">текст задачи</th>
            <th data-field="stat" data-sortable="true" data-formatter="statFormatter">статус</th>
        </tr>
        </thead>
    </table>
</div>

<script>
    function statFormatter(value, row) {
        var stat = value === '1' ? 'выполнено' : '';
        stat += row.adm === '1' ? ', <small>отредактировано администратором</small>' : '';
        return  stat;
    }
</script>







