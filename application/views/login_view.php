<div class="container">
    <h2>Страница авторизации</h2>
    <form action="" method="post">
        <div class="form-group">
            <label for="login">Логин:</label>
            <input type="text" class="form-control" id="login" name="login" required>
        </div>
        <div class="form-group">
            <label for="password">Пароль:</label>
            <input type="password" class="form-control" id="password" name="password" required>
        </div>
        <button type="submit" class="btn btn-primary">Войти</button>
    </form>

<?php extract($data); ?>
<?php if($login_status=="access_granted") { ?>
<p style="color:green">Авторизация прошла успешно.</p>
<?php } elseif($login_status=="access_denied") { ?>
    <hr/>
    <div class="alert alert-danger">
        <strong>Внимание!</strong> Логин и/или пароль введены неверно.
    </div>
<?php } ?>
</div>